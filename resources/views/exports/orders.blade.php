<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Report</title>
</head>
<body>
<table>
    <thead>
    <tr>
        <td colspan="11"
            style="font-size: 16px; height: 35px; vertical-align: center; text-align: center; font-weight: 600;">
            {{$warehouse->name}}, {{$warehouse->email}}, {{$warehouse->street_address_1}}, {{$warehouse->street_address_2}}, {{$warehouse->city}}, {{$warehouse->postal_code}}, {{$warehouse->phone}}
        </td>
    </tr>
    <tr>
        <td colspan="11" style="height: 25px; vertical-align: center; text-align: center; font-size: 13px;">
            {{$today}}
        </td>
    </tr>
    <tr>
        <td style="width: 200px; background-color: #828181;">ORDER #</td>
        <td style="width: 200px; background-color: #828181;">DATE</td>
        <td style="width: 500px; background-color: #828181;">CUSTOMER</td>
        <td style="width: 200px; background-color: #828181;">PAYMENT STATUS</td>
        <td style="width: 200px; background-color: #828181;">FULFILLMENT STATUS</td>
        <td style="width: 200px; background-color: #828181;">PRODUCT NAME</td>
        <td style="width: 200px; background-color: #828181;">PRODUCT SKU</td>
        <td style="width: 200px; background-color: #828181; text-align: right">QUANTITY</td>
        <td style="width: 200px; background-color: #828181; text-align: right">FULFILL</td>
        <td style="width: 200px; background-color: #828181; text-align: right">AMOUNT</td>
        <td style="width: 200px; background-color: #828181; text-align: right">TOTAL</td>
    </tr>
    </thead>
    <tbody>
    @php
        $orderData = [];
        $orderTotal = 0.00;
    @endphp

    @foreach($orders as $order)
        @php
            $orderItemTotal = 0.00;
        @endphp
        @foreach($order->orderItems as $orderItem)
            <tr>
                <td style="text-align: left">
                    @if(!in_array($order->order_no, $orderData))
                        #{{ $order->order_no }}
                    @endif
                </td>
                <td>
                    @if(!in_array($order->order_no, $orderData))
                        {{ $order->created_at }}
                    @endif
                </td>
                <td>
                    @if(!in_array($order->order_no, $orderData))
                        {{ $order->user ?  $order->user->first_name : '' }} {{ $order->user ? $order->user->last_name : '' }}
                    @endif
                </td>
                <td>
                    @if(!in_array($order->order_no, $orderData))
                        {{ $order->payments[0]->status }}
                    @endif
                </td>
                <td>
                    @if(!in_array($order->order_no, $orderData))
                        {{ $order->payments[0]->order_status }}
                    @endif
                </td>
                <td>
                    {{ $orderItem->product_name }}
                </td>
                <td>
                    {{ $orderItem->product_sku }}
                </td>
                <td>
                    {{ $orderItem->quantity_ordered }}
                </td>
                <td>
                    {{ $orderItem->quantity_fulfilled }}
                </td>
                <td style="text-align: right;" data-format="#,##0.00">
                    {{ $orderItem->total_price_gross_amount }}
                </td>
            </tr>
            @php
                if(!in_array($order->order_no, $orderData)) {
                    array_push($orderData, $order->order_no);
                }
                $orderTotal = $orderTotal + $orderItem->total_price_gross_amount;
                $orderItemTotal = $orderItemTotal + $orderItem->total_price_gross_amount;
            @endphp
        @endforeach
        @if($orderItemTotal > 0.00)
        <tr>
            <td colspan="9" style="background-color: #b2aaaa;"></td>
            <td style="text-align: right;font-weight: 600;background-color: #b2aaaa;"> #{{ $order->order_no }} - Total</td>
            <td style="text-align: right;font-weight: 600;background-color: #b2aaaa;" data-format="#,##0.00">{{$orderItemTotal}}</td>
        </tr>
        @endif
    @endforeach
    @if($orderTotal > 0.00)
    <tr>
        <td colspan="9" style="background-color: #828181;"></td>
        <td style="text-align: right;font-weight: 600;background-color: #828181;"> Total</td>
        <td style="text-align: right;font-weight: 600;background-color: #828181;" data-format="#,##0.00">{{$orderTotal}}</td>
    </tr>
    @endif
    </tbody>
</table>

</body>
</html>
