<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model
{
    use HasFactory;

    protected $table        = 'UserAddress';
    protected $primaryKey   = 'user_address_id';
    public    $incrementing = false;
}
