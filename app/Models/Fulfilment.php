<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fulfilment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table        = 'Fulfilment';
    protected $primaryKey   = 'fulfilment_id';
    public    $incrementing = false;

    protected $fillable = [
        'status'
    ];
}
