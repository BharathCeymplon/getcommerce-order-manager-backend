<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variant extends Model
{
    use HasFactory, SoftDeletes;

    protected $table        = 'Variant';
    protected $primaryKey   = 'Variant_id';
    public    $incrementing = false;

    public function warehouseStock()
    {
        return $this->belongsToMany(Warehouse::class, 'WarehouseStockPivot', 'variant_id', 'warehouse_id')
            ->withPivot(['quantity', 'warehouse_id', 'variant_id']);
    }
}
