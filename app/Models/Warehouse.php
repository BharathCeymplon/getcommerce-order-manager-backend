<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    use HasFactory, SoftDeletes;

    protected $table        = 'Warehouse';
    protected $primaryKey   = 'warehouse_id';
    public    $incrementing = false;

    public function warehouseStock()
    {
        return $this->belongsToMany(Variant::class, 'WarehouseStockPivot', 'warehouse_id', 'variant_id')
            ->withPivot(['quantity', 'warehouse_id', 'variant_id']);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'warehouse_id');
    }
}
