<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table        = 'Payment';
    protected $primaryKey   = 'payment_id';
    public    $incrementing = false;

    protected $fillable = [
        "status",
        "paid_amount",
        "outstanding_amount",
        "reference_no",
        "order_status"
    ];
}
