<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FulfilmentItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $table        = 'FulfilmentItem';
    protected $primaryKey   = 'fulfilment_item_id';
    public    $incrementing = false;

    protected $fillable = [
        'quantity'
    ];
}
