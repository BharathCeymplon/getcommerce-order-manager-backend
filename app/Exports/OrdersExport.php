<?php

namespace App\Exports;

use App\Models\Order;
use App\Models\User;
use App\Models\Warehouse;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

class OrdersExport implements FromView
{
    public function view(): View
    {
        $userId = Auth::user()->user_id;
        $user = User::findOrFail($userId);
        $wareHouse = Warehouse::where('email', 'like', $user->email)->with('warehouseStock')->with('payments')->first();
        $orders = Order::whereHas('orderItems')->with(['orderItems' => function ($q) use ($wareHouse) {
            return $q->whereIn('variant_id', $wareHouse->warehouseStock->pluck('variant_id')->toArray());
        }])->with(['user']);

        $orders = $orders->whereHas('payments')->with(['payments' => function ($q) use ($wareHouse) {
            return $q->where('warehouse_id', $wareHouse->warehouse_id);
        }])->orderBy('order_no', 'DESC')->get();
        date_default_timezone_set("Asia/colombo");
        return view('exports.orders', [
            'orders' => $orders,
            'warehouse' => $wareHouse,
            'today' => date("Y-m-d h:i:s a")
        ]);
    }
}
