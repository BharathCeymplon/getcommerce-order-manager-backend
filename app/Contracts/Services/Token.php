<?php

namespace App\Contracts\Services;

interface Token
{
    /**
     * Decode the Json Web Token
     *
     * @param string $jwt Json Web Token
     * @return array
     */
    public function jwtDecode($jwt);
}