<?php

namespace App\Contracts\Repositories;

interface OrderManagerRepository
{
    public function filterOrder();
    public function exportReport();
    public function showOrder($orderId);
    public function showOrderItems($orderId);
    public function fulfillmentOrder($request);
    public function paymentOrder($request);
}
