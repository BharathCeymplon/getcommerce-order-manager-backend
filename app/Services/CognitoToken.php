<?php

namespace App\Services;

use App\Contracts\Services\Token;
use App\Exceptions\InvalidAuthTokenException;
use CoderCat\JWKToPEM\JWKConverter;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Throwable;

class CognitoToken implements Token
{
    /**
     * Decode the Json Web Token
     *
     * @param $jwt
     * @return array
     * @throws InvalidAuthTokenException
     */
    public function jwtDecode($jwt)
    {
        $decodedToken = [];
        $now = now()->timestamp;

        $region = env('AWS_COGNITO_REGION');
        $clientId = env('AWS_COGNITO_CLIENT_ID');
        $userPoolId = env('AWS_COGNITO_USER_POOL_ID');

        $tokenSegments = explode('.', $jwt);
        $header = isset($tokenSegments[0])
            ? json_decode(base64_decode($tokenSegments[0]))
            : null;

        // If Token Segments != 3, do not continue processing
        if (count($tokenSegments) != 3) {
            throw new InvalidAuthTokenException('Token has wrong number of segments');
        }

        // Local kid
        $localKid = $header->kid;

        // Public kid
        $publicJwk = null;

        try {
            $decodedResponse = cache()->rememberForever('cognito-idp.' . md5($jwt), function () use ($region, $userPoolId) {
                $client = new Client();
                $response = $client->get('https://cognito-idp.' . $region . '.amazonaws.com/' . $userPoolId . '/.well-known/jwks.json');
                return json_decode($response->getBody()->getContents(), true);
            });
        } catch(Throwable $e) {
            throw new InvalidAuthTokenException($e->getMessage());
        }

        foreach ($decodedResponse['keys'] as $jwk) {
            if ($jwk['kid'] == $localKid) {
                $publicJwk = $jwk;
                break;
            }
        }

        try {
            $jwkConverter = new JWKConverter();
            $pem = $jwkConverter->toPEM($publicJwk);
            $decodedToken = JWT::decode($jwt, $pem, [$publicJwk['alg']]);
        } catch(Throwable $e) {
            throw new InvalidAuthTokenException('Token has already expired');
        }

        // Verify that the token is not expired
        if (isset($decodedToken->iat) && $decodedToken->iat > $now) {
            throw new InvalidAuthTokenException('Token cannot be issued after current time');
        }

        if (isset($decodedToken->exp) && $now >= $decodedToken->exp) {
            throw new InvalidAuthTokenException('Token has already expired');
        }

        // Verify that the audience (aud) claim matches the app client ID created in the Amazon Cognito user pool.
        if (isset($decodedToken->aud) && $decodedToken->aud != $clientId) {
            throw new InvalidAuthTokenException('Token does not match audience');
        }

        // Verify that the issuer (iss) claim matches your user pool.
        if (isset($decodedToken->iss) && $decodedToken->iss != 'https://cognito-idp.' . $region . '.amazonaws.com/' . $userPoolId) {
            throw new InvalidAuthTokenException('Token does not match user pool');
        }

        // Verify that the token_use claim matches 'id'
        // if (isset($decodedToken->token_use) && $decodedToken->token_use != 'id') {
        //    throw new InvalidAuthTokenException('Token use claim is incorrect');
        // }

        return (array) $decodedToken;
    }
}
