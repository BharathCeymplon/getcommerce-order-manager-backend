<?php

namespace App\Http\Middleware;

use App\Contracts\Services\Token;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Throwable;

class AuthenticateWithCognito
{
    /**
     * Token service instance
     *
     * @var \App\Contracts\Services\Token
     */
    private $token;

    /**
     * Handle an incoming request.
     *
     * @param \App\Contracts\Services\Token $token
     * @return void
     */
    public function __construct(Token $token)
    {
        $this->token = $token;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $token = str_replace('Bearer ', '', $request->header('AUTHORIZATION'));
            $sub = Arr::get($this->token->jwtDecode($token), 'sub');
            $user = User::where('user_id', $sub)->firstOrFail();
            Auth::onceUsingId($user->user_id);
            Auth::setUser($user);
            return $next($request);
        } catch (Throwable $e) {
            logger($e->getMessage());
            abort(401, $e->getMessage());
        }
    }
}
