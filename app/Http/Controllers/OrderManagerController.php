<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\OrderManagerRepository;
use Illuminate\Http\Request;


class OrderManagerController extends Controller
{
    protected $orderManagerRepository;

    public function __construct(OrderManagerRepository $orderManagerRepository)
    {
        $this->orderManagerRepository = $orderManagerRepository;
    }

    public function index(){
        return $this->orderManagerRepository->filterOrder();
    }

    public function report(){
        return $this->orderManagerRepository->exportReport();
    }

    public function show($orderId){
        return $this->orderManagerRepository->showOrder($orderId);
    }

    public function showOrderItems($orderId){
        return $this->orderManagerRepository->showOrderItems($orderId);
    }

    public function fulfillment(Request $request) {
        return $this->orderManagerRepository->fulfillmentOrder($request);
    }
    public function payment(Request $request) {
        return $this->orderManagerRepository->paymentOrder($request);
    }
}
