<?php

namespace App\Providers;

use App\Contracts\Services\Token;
use App\Http\Middleware\AuthenticateWithCognito;
use App\Services\CognitoToken;
use Illuminate\Support\ServiceProvider;
use App\Contracts\Repositories\OrderManagerRepository;
use App\Repositories\DefaultOrderManagerRepository;

class AppServiceProvider extends ServiceProvider
{
    public array $bindings = [
        OrderManagerRepository::class => DefaultOrderManagerRepository::class,
        Token::class => CognitoToken::class
    ];
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
