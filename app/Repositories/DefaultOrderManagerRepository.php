<?php

namespace App\Repositories;

use App\Contracts\Repositories\OrderManagerRepository;
use App\Exports\OrdersExport;
use App\Models\Fulfilment;
use App\Models\FulfilmentItem;
use App\Models\Order;
use App\Models\OrderAddress;
use App\Models\OrderItem;
use App\Models\Payment;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\WarehouseStockPivot;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Matrix\Exception;


class DefaultOrderManagerRepository implements OrderManagerRepository
{
    private function orderStruct($orders, $wareHouse)
    {
        $orders = $orders->get();
        $orders->map(function ($order) use ($wareHouse) {
            $total = $order->orderItems->sum(function ($orderItem) {
                return $orderItem->total_price_gross_amount;
            });
            $order->orderItems->map(function ($orderItem) use ($wareHouse, $total) {
                $payment = Payment::where('order_id', $orderItem->order_id)->where('warehouse_id', $wareHouse->warehouse_id)->first();
                if (!$payment) {
                    $newPayment = new Payment();
                    $newPayment->payment_id = Str::uuid()->toString();
                    $newPayment->order_id = $orderItem->order_id;
                    $newPayment->warehouse_id = $wareHouse->warehouse_id;
                    $newPayment->outstanding_amount = $total;
                    $newPayment->total_amount = $total;
                    $newPayment->save();
                }
            });
        });
    }

    public function filterOrder()
    {
        $userId = Auth::user()->user_id;
        $user = User::findOrFail($userId);
        $wareHouse = Warehouse::where('email', 'like', $user->email)->with('warehouseStock')->with('payments')->first();
        $orders = Order::whereHas('orderItems')->with(['orderItems' => function ($q) use ($wareHouse) {
            return $q->whereIn('variant_id', $wareHouse->warehouseStock->pluck('variant_id')->toArray());
        }])->with(['user']);

        $this->orderStruct($orders, $wareHouse);

        $orders = $orders->whereHas('payments')->with(['payments' => function ($q) use ($wareHouse) {
            return $q->where('warehouse_id', $wareHouse->warehouse_id);
        }])->orderBy('order_no', 'DESC')->get();

        $order['orders'] = $orders->toArray();
        $order['warehouse'] = $wareHouse->toArray();

        return response($order);
    }

    public function showOrder($orderId)
    {
        return $this->showOrderStruct($orderId);
    }

    private function showOrderStruct($orderId)
    {
        $userId = Auth::user()->user_id;
        $user = User::findOrFail($userId);
        $wareHouse = Warehouse::where('email', 'like', $user->email)->with('warehouseStock')->with('payments')->first();
        $anOrder = Order::whereHas('orderItems')->with(['orderItems' => function ($q) use ($wareHouse) {
            return $q->whereIn('variant_id', $wareHouse->warehouseStock->pluck('variant_id')->toArray());
        }])->whereHas('payments')->with(['payments' => function ($q) use ($wareHouse) {
            return $q->where('warehouse_id', $wareHouse->warehouse_id);
        }])->with(['user'])->find($orderId);

        $order['order'] = $anOrder->toArray();

        $fulfilments = Fulfilment::where('order_id', $orderId)->where('status', 'FULFILLED')->get();
        $fulfilmentIds = $fulfilments->pluck('fulfilment_id')->toArray();

        $order['fulfilments'] = $anOrder->orderItems->map(function ($orderItem) use ($wareHouse, $fulfilmentIds) {
            $items = FulfilmentItem::whereIn('fulfilment_id', $fulfilmentIds)->where('order_item_id', $orderItem->order_item_id)->get();
            return $items->map(function ($item) use ($orderItem) {
                return [
                    'id' => $item->fulfilment_item_id,
                    'fulfilment_quantity' => $item->quantity,
                    'product_name' => $orderItem->product_name,
                    'product_sku' => $orderItem->product_sku,
                    'unit_price' => $orderItem->unit_price,
                    'total_fulfil_price' => $orderItem->unit_price * $item->quantity
                ];
            });
        })->collapse();

        $order['wareHouse'] = $wareHouse->toArray();

        $shipping = [];
        $billing = [];
        if ($anOrder->shipping_order_address_id) {
            $shipping = OrderAddress::findOrFail($anOrder->shipping_order_address_id);
        }
        if ($anOrder->billing_order_address_id) {
            $billing = OrderAddress::findOrFail($anOrder->billing_order_address_id);
        }

        $order['address'] = [
            'BILLING' => $billing,
            'SHIPPING' => $shipping
        ];

        return response($order);
    }

    public function showOrderItems($orderId)
    {
        return $this->fulfilment($orderId);
    }

    private function fulfilment($orderId)
    {
        $userId = Auth::user()->user_id;
        $user = User::findOrFail($userId);
        $wareHouse = Warehouse::where('email', 'like', $user->email)->with('warehouseStock')->with('payments')->first();
        $order = Order::whereHas('orderItems')->with(['orderItems' => function ($q) use ($wareHouse) {
            return $q->whereIn('variant_id', $wareHouse->warehouseStock->pluck('variant_id')->toArray());
        }])->with(['user'])->find($orderId);

        $fulfilmentItems['orderItems'] = $order->orderItems->map(function ($orderItem) use ($wareHouse) {
            $stock = $this->stock($wareHouse->warehouseStock, $orderItem->variant_id);
            return [
                'order_id' => $orderItem->order_id,
                'order_item_id' => $orderItem->order_item_id,
                'variant_id' => $orderItem->variant_id,
                'product_name' => $orderItem->product_name,
                'sku' => $orderItem->product_sku,
                'to_fulfil' => $orderItem->quantity_ordered - $orderItem->quantity_fulfilled,
                'stock' => $stock,
                'quantity_ordered' => $orderItem->quantity_ordered,
                'warehouse_id' => $wareHouse->warehouse_id,
            ];
        });
        $fulfilmentItems['wareHouse'] = $wareHouse->toArray();
        $fulfilmentItems['order'] = $order->toArray();

        return response($fulfilmentItems);
    }

    private function stock($wareHouseSock, $variantId)
    {
        $stock = $wareHouseSock->where('variant_id', $variantId)->first();
        return $stock->pivot->quantity;
    }

    public function fulfillmentOrder($request)
    {
        $orderItems = $request->input('orderItems');
        $order = $request->input('order');
        $userId = Auth::user()->user_id;
        $user = User::findOrFail($userId);
        $wareHouse = Warehouse::where('email', 'like', $user->email)->with('warehouseStock')->first();
        $warehouse_id = $wareHouse->warehouse_id;

        foreach ($orderItems as $orderItem) {
            $allowed = ['to_fulfil'];
            $filtered = array_filter(
                $orderItem,
                function ($key) use ($allowed) {
                    return in_array($key, $allowed);
                },
                ARRAY_FILTER_USE_KEY
            );
            if ($filtered['to_fulfil'] > 0) {
                $fulfilment = new Fulfilment();
                $fulfilment->fulfilment_id = Str::uuid()->toString();
                $fulfilment->order_id = $order['order_id'];
                $fulfilment->status = "FULFILLED";
                $fulfilment->tracking_reference = null;
                $fulfilment->save();

                $bool = true;
                foreach ($orderItems as $item) {
                    $one = OrderItem::find($item['order_item_id']);
                    if ($item['to_fulfil'] > 0) {
                        $newFulfilled = $one->quantity_fulfilled + $item['to_fulfil'];

                        if (($newFulfilled <= $item['stock']) && ($newFulfilled <= $one->quantity_ordered)) {
                            $fulfilmentItem = new FulfilmentItem();
                            $fulfilmentItem->fulfilment_item_id = Str::uuid()->toString();
                            $fulfilmentItem->quantity = $item['to_fulfil'];
                            $fulfilmentItem->fulfilment_id = $fulfilment->fulfilment_id;
                            $fulfilmentItem->order_item_id = $item['order_item_id'];
                            $fulfilmentItem->save();
                            $one->increment('quantity_fulfilled', $item['to_fulfil']);
                            WarehouseStockPivot::where('variant_id', $item['variant_id'])
                                ->where('warehouse_id', $item['warehouse_id'])->where('quantity', '>', 0)->decrement('quantity', $item['to_fulfil']);
                        }
                    }
                    if ($one->quantity_ordered !== $one->quantity_fulfilled) {
                        $bool = false;
                    }
                }
                $payment = Payment::where('order_id', $order['order_id'])->where('warehouse_id', $warehouse_id);

                $orderStatus = $bool ? 'FULFILLED' : 'UN FULFILLED';
                $payment->update(['order_status' => $orderStatus]);

                return $this->orderStatusUpdate($order['order_id']);
            }
        }
    }

    private function orderStatusUpdate($order_id)
    {
        $order = Order::findOrFail($order_id);

        if (!$order) return $this->showOrderStruct($order_id);

//        TODO warehouse vice (we need to load default payment information for every warehouse, when a society login first, but for now we have loaded for a warehouse who login not every one)
//        $count = Payment::where('order_id',$order->order_id)->where('order_status', 'UN FULFILLED')->count();

//        TODO order item vice
        $count = $order->orderItems->filter(function ($item) {
            return $item->quantity_ordered !== $item->quantity_fulfilled;
        })->count();

        $orderStatus = $count > 0 ? 'UNFULFILLED' : 'FULFILLED';
        $order->update(['status' => $orderStatus]);

        return $this->showOrderStruct($order_id);
    }

    public function paymentOrder($request)
    {
        $payment = $request->input('payment');
        $paid_amount = floatval(preg_replace("/[^-0-9\.]/", "", $payment['paid_amount']));
        $referenceNo = $payment['reference_no'];
        $total = $payment['total_amount'];

        $payment = Payment::findOrFail($payment['payment_id']);

        $payment->increment('paid_amount', $paid_amount);
        $payment->decrement('outstanding_amount', $paid_amount);
        $outstanding_amount = $payment->outstanding_amount;
        $payment->update([
            'status' => ($outstanding_amount === $total) ? 'UN PAID' : (($outstanding_amount < $total && $outstanding_amount > 0.00) ? "PARTIAL" : 'PAID'),
            'reference_no' => $referenceNo
        ]);
        Order::findOrFail($payment->order_id)->increment('total_paid_amount', $paid_amount);
    }

    public function exportReport()
    {
        $userId = Auth::user()->user_id;
        $user = User::findOrFail($userId);
        $wareHouse = Warehouse::where('email', 'like', $user->email)->first();
        date_default_timezone_set("Asia/colombo");
        $fileName = $wareHouse->name . '-' . date("Y-m-d h:i:s a") . '.xlsx';
        $xls = Excel::download(new OrdersExport,  $fileName, \Maatwebsite\Excel\Excel::XLSX)->getFile()->getContent();
        return response([
            'data' => base64_encode($xls),
            'fileName' => $fileName
        ]);
    }
}
