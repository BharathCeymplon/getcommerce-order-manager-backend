<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Payment', function (Blueprint $table) {
            $table->char('payment_id', 36)->nullable(false);
            $table->char('reference_no', 255)->nullable(true);
            $table->char('order_id', 36)->nullable(false);
            $table->char('warehouse_id', 36)->nullable(false);
            $table->float('paid_amount')->nullable(false)->default(0.00);
            $table->float('total_amount')->nullable(false)->default(0.00);
            $table->float('outstanding_amount')->nullable(false)->default(0.00);
            $table->enum('status', ['PAID', 'UN PAID', 'PARTIAL'])->nullable(false)->default('UN PAID');
            $table->enum('order_status', ['UN FULFILLED', 'FULFILLED'])->nullable(false)->default('UN FULFILLED');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Payment');
    }
}
