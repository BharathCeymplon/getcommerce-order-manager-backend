<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['authWithCognito']], function (Router $router) {
    $router->get('orders',\App\Http\Controllers\OrderManagerController::class.'@index');
    $router->post('orders/report',\App\Http\Controllers\OrderManagerController::class.'@report');
    $router->get('order/{orderId}',\App\Http\Controllers\OrderManagerController::class.'@show');
    $router->get('order/items/{orderId}',\App\Http\Controllers\OrderManagerController::class.'@showOrderItems');
    $router->post('order/fulfillment',\App\Http\Controllers\OrderManagerController::class.'@fulfillment');
    $router->post('order/payment',\App\Http\Controllers\OrderManagerController::class.'@payment');
});


